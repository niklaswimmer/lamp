packer {
    required_plugins {
        puppet = {
            version = ">= 1.0.0"
            source  = "github.com/hashicorp/puppet"
        }
        vagrant = {
            version = ">= 1.0.0"
            source  = "github.com/hashicorp/vagrant"
        }
    }
}

source "vagrant" "focal" {
    communicator = "ssh"
    ssh_timeout = "1m"

    source_path = "ubuntu/focal64"
    box_version = "20210610.0.0"
    provider = "virtualbox"

    template = "vagrant/template"

    teardown_method = "destroy"

    output_dir = "output-focal"

    output_vagrantfile = "./package-files/Vagrantfile"
    package_include = ["./package-files/info.json"]
}

build {
    name = "lamp"

    sources = ["source.vagrant.focal"]

    provisioner "shell" {
        inline = [
            "sudo wget -q https://apt.puppet.com/puppet7-release-focal.deb",
            "sudo dpkg -i puppet7-release-focal.deb >> /dev/null",
            "sudo apt-get update -yqq",
            "sudo apt-get install -yqq puppet",
        ]
    }

    provisioner "puppet-masterless" {
        manifest_file = "puppet/manifests/"
        module_paths = ["puppet/modules/"]
    }

    provisioner "shell" {
        inline = [
            "sudo apt-get purge -yqq puppet",
            "sudo apt-get purge -yqq puppet7-release",
            "sudo apt-get autoremove -yqq --purge",
            "sudo apt-get clean -yqq",
            "sudo rm puppet7-release-focal.deb",
        ]
    }
}
