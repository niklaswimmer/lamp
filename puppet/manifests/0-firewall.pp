include ufw

ufw::allow {
  'ssh':
    port => '22',
  ;
  'http':
    port => '80',
  ;
  'mysql':
    port => '3306',
  ;
}
