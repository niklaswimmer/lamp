class { 'apache':
  default_vhost => false,
  mpm_module    => 'prefork',
}

include apache::mod::php

file {
  default:
    owner => 'vagrant',
    group => 'vagrant',
  ;
  ['/vagrant', '/vagrant/web', '/vagrant/log']:
    ensure => 'directory',
  ;
  '/vagrant/web/index.php':
    ensure  => 'file',
    content => '<?php phpinfo();',
  ;
}

apache::vhost { 'vagrant':
  port          => '80',
  docroot       => '/vagrant/web',
  docroot_owner => 'vagrant',
  docroot_group => 'vagrant',
  logroot       => '/vagrant/log',
}
